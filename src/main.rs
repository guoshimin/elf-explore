#![feature(type_ascription)]
// use std::fs;
use std::fs::File;
// use& std::io::Read;
use memmap::MmapOptions;
use null_terminated::NulStr;
use num_derive::FromPrimitive;
use std::env;
use std::ops::Index;
use strum::IntoEnumIterator;
use strum_macros::{AsRefStr, EnumIter};

// macro_rules! enum_str {
//     (enum $name:ident {
//         $($variant:ident = $val:expr),*,
//     }) => {
//         enum $name {
//             $($variant = $val),*
//         }

//         impl $name {
//             fn name(&self) -> &'static str {
//                 match self {
//                     $($name::$variant => stringify!($variant)),*
//                 }
//             }
//         }
//     };
// }

#[repr(C)]
struct Elf64Ehdr {
    e_ident: [u8; IdentOffset::EI_NIDENT as usize],
    e_type: u16,
    e_machine: u16,
    e_version: u32,
    e_entry: u64,
    e_phoff: u64,
    e_shoff: u64,
    e_flags: u32,
    e_ehsize: u16,
    e_phentsize: u16,
    e_phnum: u16,
    e_shentsize: u16,
    e_shnum: u16,
    e_shstrndx: u16,
}

#[derive(Debug, AsRefStr, EnumIter, Copy, Clone)]
#[repr(usize)]
#[allow(non_camel_case_types)]
enum IdentOffset {
    EI_MAG0 = 0,
    EI_MAG1 = 1,
    EI_MAG2 = 2,
    EI_MAG3 = 3,
    EI_CLASS = 4,
    EI_DATA = 5,
    EI_VERSION = 6,
    EI_OSABI = 7,
    EI_ABIVERSION = 8,
    EI_PAD = 9,
    EI_NIDENT = 16,
}

#[derive(Debug, AsRefStr, FromPrimitive)]
#[repr(u8)]
enum EIClass {
    ELFCLASSNONE,
    ELFCLASS32,
    ELFCLASS64,
}

#[derive(Debug, AsRefStr, FromPrimitive)]
#[repr(u8)]
enum EIData {
    ELFDATANONE,
    ELFDATA2LSB,
    ELFDATA2MSB,
}

#[derive(Debug, AsRefStr, FromPrimitive)]
#[repr(u8)]
#[allow(non_camel_case_types)]
enum EIOsabi {
    ELFOSABI_NONE,
    ELFOSABI_HPUX,
    ELFOSABI_NETBSD,
    ELFOSABI_LINUX,
    ELFOSABI_SOLARIS = 6,
    ELFOSABI_AIX,
    ELFOSABI_IRIX,
    ELFOSABI_FREEBSD,
    ELFOSABI_TRU64,
    ELFOSABI_MODESTO,
    ELFOSABI_OPENBSD,
    ELFOSABI_OPENVMS,
    ELFOSABI_NSK,
}

#[derive(Debug, AsRefStr, FromPrimitive)]
#[repr(u16)]
#[allow(non_camel_case_types)]
enum EType {
    ET_NONE,
    ET_REL,
    ET_EXEC,
    ET_DYN,
    ET_CORE,
    ET_LOOS = 0xfe00,
    ET_HIOS = 0xfeff,
    ET_LOPROC = 0xff00,
    ET_HIPROC = 0xffff,
}

#[repr(C)]
struct Elf64Shdr {
    sh_name: u32,
    sh_type: u32,
    sh_flags: u64,
    sh_addr: u64,
    sh_offset: u64,
    sh_size: u64,
    sh_link: u32,
    sh_info: u32,
    sh_addralign: u64,
    sh_entsize: u64,
}

#[derive(Debug, AsRefStr, FromPrimitive)]
#[repr(u32)]
#[allow(non_camel_case_types)]
enum SH_TYPE {
    SHT_NULL = 0,                    /* inactive */
    SHT_PROGBITS = 1,                /* program defined information */
    SHT_SYMTAB = 2,                  /* symbol table section */
    SHT_STRTAB = 3,                  /* string table section */
    SHT_RELA = 4,                    /* relocation section with addends */
    SHT_HASH = 5,                    /* symbol hash table section */
    SHT_DYNAMIC = 6,                 /* dynamic section */
    SHT_NOTE = 7,                    /* note section */
    SHT_NOBITS = 8,                  /* no space section */
    SHT_REL = 9,                     /* relocation section - no addends */
    SHT_SHLIB = 10,                  /* reserved - purpose unknown */
    SHT_DYNSYM = 11,                 /* dynamic symbol table section */
    SHT_INIT_ARRAY = 14,             /* Initialization function pointers. */
    SHT_FINI_ARRAY = 15,             /* Termination function pointers. */
    SHT_PREINIT_ARRAY = 16,          /* Pre-initialization function ptrs. */
    SHT_GROUP = 17,                  /* Section group. */
    SHT_SYMTAB_SHNDX = 18,           /* Section indexes (see SHN_XINDEX). */
    SHT_LOOS = 0x60000000,           /* First of OS specific semantics */
    SHT_GNU_ATTRIBUTES = 0x6ffffff5, /* GNU object attributes */
    SHT_GNU_HASH = 0x6ffffff6,       /* GNU hash table */
    SHT_GNU_LIBLIST = 0x6ffffff7,    /* GNU prelink library list */
    SHT_GNU_VERDEF = 0x6ffffffd,     /* GNU version definition section */
    SHT_GNU_VERNEED = 0x6ffffffe,    /* GNU version needs section */
    SHT_GNU_VERSYM = 0x6fffffff,     /* GNU version symbol table */
    // SHT_HIOS = 0x6fffffff /* Last of OS specific semantics */,
    SHT_LOPROC = 0x70000000,        /* reserved range for processor */
    SHT_MIPS_ABIFLAGS = 0x7000002a, /* .MIPS.abiflags */
    SHT_HIPROC = 0x7fffffff,        /* specific section header types */
    SHT_LOUSER = 0x80000000,        /* reserved range for application */
    SHT_HIUSER = 0xffffffff,        /* specific indexes */
}

#[repr(C)]
struct Elf64_Rela {
    r_offset: u64, /* Location to be relocated. */
    r_info: u64,   /* Relocation type and symbol index. */
    r_addend: i64, /* Addend. */
}

#[repr(C)]
struct Elf64_Sym {
    st_name: u32,
    st_info: u8,
    st_other: u8,
    st_shndx: u16,
    st_value: u64,
    st_size: u64,
}

#[derive(Debug, AsRefStr, FromPrimitive)]
#[repr(u32)]
#[allow(non_camel_case_types)]
enum REL_TYPE {
    R_X86_64_NONE = 0,
    R_X86_64_64 = 1,
    R_X86_64_PC32 = 2,
    R_X86_64_GOT32 = 3,
    R_X86_64_PLT32 = 4,
    R_X86_64_COPY = 5,
    R_X86_64_GLOB_DAT = 6,
    R_X86_64_JUMP_SLOT = 7,
    R_X86_64_RELATIVE = 8,
    R_X86_64_GOTPCREL = 9,
    R_X86_64_32 = 10,
    R_X86_64_32S = 11,
    R_X86_64_16 = 12,
    R_X86_64_PC16 = 13,
    R_X86_64_8 = 14,
    R_X86_64_PC8 = 15,
    R_X86_64_DPTMOD64 = 16,
    R_X86_64_DTPOFF64 = 17,
    R_X86_64_TPOFF64 = 18,
    R_X86_64_TLSGD = 19,
    R_X86_64_TLSLD = 20,
    R_X86_64_DTPOFF32 = 21,
    R_X86_64_GOTTPOFF = 22,
    R_X86_64_TPOFF32 = 23,
}

impl Index<IdentOffset> for [u8] {
    type Output = u8;

    fn index(&self, idx: IdentOffset) -> &Self::Output {
        &self[idx as usize]
    }
}

// fn get_file_as_byte_vec(filename: &String) -> Vec<u8> {
//     let mut f = File::open(&filename).expect("no file found");
//     let metadata = fs::metadata(&filename).expect("unable to read metadata");
//     let mut buffer = vec![0; metadata.len() as usize];
//     f.read(&mut buffer).expect("buffer overflow");

//     buffer
// }

fn print_field<T: num::traits::FromPrimitive + AsRef<str>>(buf: &[u8], offset: IdentOffset) {
    let d: T = num::FromPrimitive::from_u8(buf[offset]).unwrap();
    let field_name: &str = offset.as_ref();
    let data_name: &str = d.as_ref();
    println!("{}: {}", field_name, data_name);
}

fn display_rela(buf: *const u8, sh_rela: &Elf64Shdr, section_hdrs: &Vec<Elf64Shdr>, section_names: &Vec<*const u8>) {
    let symtab = &section_hdrs[sh_rela.sh_link as usize];
    let symtab_sec_name = unsafe { NulStr::new_unchecked(section_names[sh_rela.sh_link as usize]) };
    println!("symtab: {}", symtab_sec_name);
    let info_sec_name = unsafe { NulStr::new_unchecked(section_names[sh_rela.sh_info as usize]) };
    println!("info: {:#10x}/{}", sh_rela.sh_info, info_sec_name);
    let strtab = &section_hdrs[symtab.sh_link as usize];
    let mut sym_names: Vec<*const u8> =
        Vec::with_capacity((symtab.sh_size / symtab.sh_entsize) as usize);
    for i in (0..symtab.sh_size).step_by(symtab.sh_entsize as usize) {
        let offset = (symtab.sh_offset + i) as isize;
        let ref ent: Elf64_Sym = unsafe { std::ptr::read(buf.offset(offset) as *const _) };
        let name = unsafe { buf.offset((strtab.sh_offset + ent.st_name as u64) as isize) };
        sym_names.push(name);
    }
    for i in (0..sh_rela.sh_size).step_by(sh_rela.sh_entsize as usize) {
        let offset: isize = (sh_rela.sh_offset + i) as isize;
        let ref rela: Elf64_Rela = unsafe { std::ptr::read(buf.offset(offset) as *const _) };
        println!("r_offset: {:#018x}", rela.r_offset);
        println!("  r_info: {:#018x}", rela.r_info);
        println!("  sym: {:#010x}", rela.r_info >> 32);
        let symbol = unsafe { NulStr::new_unchecked(sym_names[(rela.r_info >> 32) as usize]) };
        println!("  sym: {}", symbol);
        println!("  type: {:#010x}", rela.r_info & 0xffffffff);
        let rel_type = match num::FromPrimitive::from_u32((rela.r_info & 0xffffffff) as u32):
            Option<REL_TYPE>
        {
            Some(rel_type) => String::from(rel_type.as_ref()),
            _ => String::from("Unknow"),
        };
        println!("  type: {}", rel_type);
        println!("  type: {}", rel_type);
        println!("  r_addend: {:#018x}", rela.r_addend);
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let f = File::open(&args[1]).expect("no file found");
    let mmap = unsafe { MmapOptions::new().map(&f).unwrap() };
    // let mut magic: [u8; IdentOffset::EI_NIDENT as usize] = [0; IdentOffset::EI_NIDENT as usize];
    // f.read(&mut magic).expect("read error");
    let magic = &mmap;
    println!("{} {} {} {}", magic[0], magic[1], magic[2], magic[3]);
    for offset in IdentOffset::iter() {
        match offset {
            IdentOffset::EI_NIDENT => break,
            _ => {
                let value = magic[offset];
                let field: &str = offset.as_ref();
                println!("{}: {}", field, value);
            }
        }
    }
    assert!(magic[IdentOffset::EI_MAG0] == 0x7f);
    assert!(magic[IdentOffset::EI_MAG1] == 'E' as u8);
    assert!(magic[IdentOffset::EI_MAG2] == 'L' as u8);
    assert!(magic[IdentOffset::EI_MAG3] == 'F' as u8);

    print_field::<EIClass>(&magic, IdentOffset::EI_CLASS);
    print_field::<EIData>(&magic, IdentOffset::EI_DATA);
    println!("EI_VERSION: {}", magic[IdentOffset::EI_VERSION]);
    print_field::<EIOsabi>(&magic, IdentOffset::EI_OSABI);
    println!("EI_ABIVERSION: {}", magic[IdentOffset::EI_ABIVERSION]);

    let ref hdr: Elf64Ehdr = unsafe { std::ptr::read(mmap.as_ptr() as *const _) };
    let e_type: EType = num::FromPrimitive::from_u16(hdr.e_type).unwrap();
    println!("e_type: {}", e_type.as_ref());
    println!("e_machine: {:#04x}", hdr.e_machine);
    println!("e_version: {:#06x}", hdr.e_version);
    println!("e_entry: {:#018x}", hdr.e_entry); // entrypoint. 0 for object files.
    println!("e_phoff: {:#018x}", hdr.e_phoff); // offset of program headers. 0 for object files.
    println!("e_phentsize: {:#04x}", hdr.e_phentsize);
    println!("e_phnum: {:#04x}", hdr.e_phnum);

    println!("e_shstrndx: {}", hdr.e_shstrndx);
    println!("e_shoff: {:#018x}", hdr.e_shoff); // offset of section headers.
    println!("e_shentsize: {:#04x}", hdr.e_shentsize);
    println!("e_shnum: {:#04x}", hdr.e_shnum);

    let shstr: Elf64Shdr = unsafe {
        std::ptr::read(
            mmap.as_ptr()
                .offset((hdr.e_shoff + (hdr.e_shstrndx * hdr.e_shentsize) as u64) as isize)
                as *const _,
        )
    };

    let str_section_offset = shstr.sh_offset as isize;
    let mut section_hdrs: Vec<Elf64Shdr> = Vec::with_capacity(hdr.e_shnum as usize);
    let mut section_names: Vec<*const u8> = Vec::with_capacity(hdr.e_shnum as usize);

    for i in 0..hdr.e_shnum {
        println!("{}", i);
        let sh: Elf64Shdr = unsafe {
            std::ptr::read(
                mmap.as_ptr()
                    .offset((hdr.e_shoff + (i * hdr.e_shentsize) as u64) as isize)
                    as *const _,
            )
        };
        println!("sh_name: {}", sh.sh_name);
        let name_cstr = unsafe {
            NulStr::new_unchecked(
                mmap.as_ptr()
                    .offset(str_section_offset + sh.sh_name as isize),
            )
        };
        section_names.push(unsafe {
            mmap.as_ptr()
                .offset(str_section_offset + sh.sh_name as isize)
        });
        println!("sh_name: {}", name_cstr);
        println!("sh_size: {}", sh.sh_size);
        println!("sh_entsize: {}", sh.sh_entsize);
        println!("sh_offset: {:#018x}", sh.sh_offset);
        println!("sh_addr: {:#018x}", sh.sh_addr);
        println!("sh_link: {}", sh.sh_link);
        println!("sh_info: {}", sh.sh_info);
        println!("sh_type: {:#010x}", sh.sh_type);
        let sh_type_name: String = match num::FromPrimitive::from_u32(sh.sh_type): Option<SH_TYPE> {
            Some(sh_type) => String::from(sh_type.as_ref()),
            _ => String::from("unknown"),
        };
        // let sh_type: SH_TYPE = num::FromPrimitive::from_u32(sh.sh_type).unwrap();
        println!("sh_type: {}", sh_type_name);
        section_hdrs.push(sh);
    }
    for (idx, sh) in section_hdrs.iter().enumerate() {
        let sh_type: SH_TYPE = num::FromPrimitive::from_u32(sh.sh_type).unwrap();
        match sh_type {
            SH_TYPE::SHT_RELA => {
		let sec_name = unsafe { NulStr::new_unchecked(section_names[idx]) };
		println!("relocation section: {}", sec_name);
		display_rela(mmap.as_ptr(), &sh, &section_hdrs, &section_names);
	    },
            _ => (),
        }
    }
}

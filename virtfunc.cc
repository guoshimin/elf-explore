#include <iostream>
#include <iomanip>

class Base {
public:
  virtual void foo() = 0;
};

class Derived : public Base {
public:
  void hello() {}
  virtual void foo() override {
  }
};

int main() {
  Base *foo = new Derived;
  void *c = (void *) foo;
  std::cout << std::hex << std::setfill('0') << std::setw(16) << c << std::endl;
  std::cout << std::hex << std::setfill('0') << std::setw(16) << *((unsigned long *) c) << std::endl;
  return 0;
}
